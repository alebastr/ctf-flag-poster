module Utils where
import System.IO;
-- local imports
import Flag

flagPrint :: FlagSubscriber
flagPrint v = putStr $ "Flag: " ++ (show v) ++ "\n"

stdioReader :: FlagSubscriber -> IO ()
stdioReader cb = do
	flag <- getLine
	cb $ newFlag flag
	stdioReader cb
