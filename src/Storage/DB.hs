module Storage.DB where
-- global imports
--import Control.Concurrent
--import Control.Monad
import Data.Maybe
import Database.HDBC
import Database.HDBC.SqlValue
import Database.HDBC.Sqlite3
import System.Log.Logger
-- local imports
import Flag
import Storage

data StorageDB = StorageDB {
	  filename :: String
	, database :: Connection
	}

initStorageDB f d = StorageDB {filename=f, database=d}

getStorageDB :: String -> IO StorageDB
getStorageDB filename = do
	conn <- connectSqlite3 filename
	return $ initStorageDB filename conn

instance StoreFlags StorageDB where
	check  (StorageDB _ d) f = do
		flag <- selectFlag d f
		debugM "storage.sqlite" $ (text f) ++ " -> " ++ (show $ isJust flag)
		if (isJust flag) 
			then if (processed $ fromJust flag)
				then do
					debugM "storage.sqlite" $ "already processed: " ++ (show $ fromJust flag)
					return Nothing
				else do
					return flag
			else
				return $ Just f
	

	update (StorageDB _ d) f = do
		ex <- existsFlag d f
		(if ex then updateFlag else insertFlag) d f

existsFlag :: IConnection conn => conn -> Flag -> IO Bool
existsFlag conn flag =
	quickQuery' conn ("SELECT COUNT(text) FROM flags WHERE text=?") [toSql $ text flag] 
		>>= return . fromSql . head . head

insertFlag :: IConnection conn => conn -> Flag -> IO ()
insertFlag conn f = do
	quickQuery' conn ("INSERT INTO flags (text, state, source, service) VALUES (?, ?, ?, ?)") $ 
		map (toSql) [
			  text f
			, maybe "Undefined" show $ state f
			, source f
			]
		++ [ maybe SqlNull (toSql) $ service f ]
	commit conn
	return ()

updateFlag :: IConnection conn => conn -> Flag -> IO ()
updateFlag conn f = do
	quickQuery' conn ("UPDATE flags SET state=?, source=? WHERE text=?") $ 
		map (toSql) [
			  maybe "Undefined" show $ state f
			, source f
			, text f
			]
	commit conn
	return ()

selectFlag :: IConnection conn => conn -> Flag -> IO (Maybe Flag)
selectFlag conn f = do
	ex <- existsFlag conn f
	if ex
		then do
			qr <- quickQuery' conn ("SELECT state FROM flags WHERE text=?") [toSql $ text f]
			return $ Just $ setStateS f $ fromSql $ head $ head qr
		else return Nothing
