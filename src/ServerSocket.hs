module ServerSocket where
import Network
import Control.Concurrent
import System.IO
import System.Log.Logger

type NetworkHandler = String -> String -> IO ()

networkListener :: Int -> NetworkHandler -> IO ()
networkListener port handler = withSocketsDo $ do
	log INFO $ "starting at port " ++ (show port)
	sock <- listenOn $ PortNumber $ fromIntegral port
	procRequests sock
	where
		log = logM "net.listener"
		procRequests :: Socket -> IO ()
		procRequests mastersock = do
			(handle, chost, cport) <- accept mastersock
			log NOTICE $ (show port) ++ ": client connection from " ++ chost ++ ":" ++ (show cport)
			forkIO $ procMessages handle chost cport 
			procRequests mastersock

		procMessages :: Handle -> HostName -> PortNumber -> IO ()
		procMessages connhdl chost cport = do
			hSetBuffering connhdl LineBuffering
			messages <- hGetContents connhdl
			mapM_ (handler chost) (lines messages)
			hClose connhdl
