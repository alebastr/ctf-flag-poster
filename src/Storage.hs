module Storage where
-- global imports
-- local imports
import Flag

data Storage = Storage


getStorage :: IO Storage
getStorage = return Storage

class StoreFlags a where
	check   :: a -> FlagProcessor
	update  :: a -> FlagSubscriber

instance StoreFlags Storage where
	check _    = return . Just
	update _ _ = return ()
	
