#!/usr/bin/runhaskell
module Concurrency where
-- global imports
import Control.Monad;
import Control.Monad.STM;
import Control.Concurrent;
import Control.Concurrent.STM.TChan;
import Data.Maybe
-- local imports
import Flag

pushFlagEvent :: TChan Flag -> Flag -> IO ()
pushFlagEvent chan flag = do
	atomically $ writeTChan chan flag

flagProcessAll :: [FlagProcessor] -> FlagProcessor
flagProcessAll processors f = foldM apply (Just f) processors
	where apply v x = maybe (return Nothing) x v

flagNotifyAll :: [FlagSubscriber] -> FlagSubscriber
flagNotifyAll subscribers f = mapM_ (apply f) subscribers
	where apply v x = x v

startConcurrent :: [FlagSubscriber] -> [FlagProcessor] -> [FlagSubscriber] -> IO FlagSubscriber
startConcurrent preSub processors postSub = do
	chan <- atomically $ newTChan
	forkIO $ mainLoop chan preSub processors postSub
	return $ pushFlagEvent chan
	where
		mainLoop :: TChan Flag -> [FlagSubscriber] -> [FlagProcessor] -> [FlagSubscriber] -> IO ()
		mainLoop chan preSub processors postSub = do
			flag <- atomically $ readTChan chan
			flagNotifyAll preSub flag
			flagProcessAll processors flag >>= maybe (return ()) (flagNotifyAll postSub)
			mainLoop chan preSub processors postSub
