module Checker.Connection.Socket where
-- global imports
import Network
import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.STM.TChan
import Control.Exception
import Text.Regex.Posix
import System.IO
import System.Log.Logger
-- local imports
import Checker
import Flag hiding (accept)

data CheckerConnectionSocket = CheckerConnectionSocket {
	--external
	  host :: String
	, port :: PortID
	-- internal
	, cin :: TChan String
	, cout:: TChan String
}

getCheckerConnectionSocket ci host portNum = do
	pipein <- atomically $ newTChan
	pipeout <- atomically $ newTChan
	tid <- forkIO $ connectJury $ jury pipein pipeout
	return $ jury pipein pipeout
	where
		port = PortNumber $ fromIntegral portNum
		jury cin cout = CheckerConnectionSocket {host=host, port=port, cin=cin, cout=cout}
		log = logM "net.checker"
		logException :: SomeException -> IO ()
		logException e = errorM "net.checker" $ "no connection to checker: " ++ (show e)
	
		connectJury js = do
			res <- try $ do
				hndl <- connectTo host port
				log INFO $ "connected to checker: " ++ host ++ ":" ++ (show portNum)
				hSetBuffering hndl LineBuffering
				checkerConnectionInit ci hndl
				inputLoop hndl (cin js) (cout js)
			case res of
				Left e -> logException e
				_ -> log INFO "reconnecting"
			waitBeforeReconnect ci
			connectJury js

		inputLoop h cin cout = do
			log DEBUG "waiting"
			line <- atomically $ readTChan cin
			log DEBUG "processing"
			response <- try $ readResponse h line
			case response of
				-- exception
				-- reconnect and process this flag again
				Left e -> do 
					atomically $ unGetTChan cin line
					logException e
				-- value
				Right x -> do
					atomically $ writeTChan cout x
					waitBeforeSubmit ci
					inputLoop h cin cout
		
		readResponse h l = do
			hPutStrLn h l
			hWaitForInput h (100)
			hGetLine h

postFlagSocket :: CheckerConnectionSocket -> Flag -> IO String
postFlagSocket j f = do
	atomically $ writeTChan (cin j) (text f)
	response <- atomically $ readTChan (cout j)
	debugM "net.checker" ((text f) ++ " -> " ++ response)
	return response

instance CheckerConnection CheckerConnectionSocket where
--	initConnection (CheckerConnectionSocket host port _ _) = getCheckerConnectionSocket host port
	postFlag j ci f = postFlagSocket j f >>= return . Just . setState f . flagState ci
