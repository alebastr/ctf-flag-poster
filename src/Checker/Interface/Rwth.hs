module Checker.Interface.Rwth where
-- global imports
import Control.Concurrent
import System.IO
import System.Log.Logger
import Text.Regex
-- local imports
import Checker
import Flag

data RwthChecker = RwthChecker

instance CheckerInterface RwthChecker where
	flagFormat _ = mkRegex "[a-z0-9]{16}"
	flagState _ s = 
		case s of
			-- rwthCTF checker responses
			"You already submitted this flag." -> Accepted
			"Congratulations, you scored a point!" -> Accepted
			"Flag validity expired." -> Stale
			"Invalid flag." -> Rejected
			"Unknown flag." -> Rejected
			"This is your own flag." -> Own
			_ -> Undefined

	waitBeforeReconnect _ = threadDelay 1000000 -- ms
	waitBeforeSubmit _ = threadDelay 100 -- ms
	-- handshake with checker
	checkerConnectionInit _ h = do
		_ <- hGetLine h
		return ()
