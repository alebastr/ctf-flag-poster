module Checker.Interface.Mock where
-- global imports
import Control.Concurrent
import System.IO
import System.Log.Logger
import Text.Regex
-- local imports
import Checker
import Flag

data MockChecker = MockChecker

instance CheckerInterface MockChecker where
	flagFormat _ = mkRegex "[a-zA-Z0-9]{31}="
	flagState _ s = 
		case s of
			-- rwthCTF checker responses
			"Accepted" -> Accepted
			"Stale" -> Stale
			"Rejected" -> Rejected
			"Own" -> Own
			_ -> Undefined

	waitBeforeReconnect _ = threadDelay 1000 -- ms
	waitBeforeSubmit _ = threadDelay 100 -- ms
	-- handshake with checker
	checkerConnectionInit _ h = do
		return ()
