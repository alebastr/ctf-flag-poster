module Flag where
--global imports
import Data.Maybe;
--local imports

--type Flag = String

type FlagProcessor = Flag -> IO (Maybe Flag)
type FlagSubscriber = Flag -> IO ()


data FlagState = New | Accepted | Own | Rejected | Stale | ServiceDown | Undefined deriving (Eq, Read, Show)

stateAccepted f = f == Accepted
stateRejected f = elem f [Own, Rejected, Stale]
stateProcessed f = stateAccepted f || stateRejected f

-- Flag definition: '\w{31}='

data Flag = Flag {
	  text :: String
	, state :: Maybe FlagState
	, service:: Maybe String
	, source :: String
--	, time   :: String
} deriving (Read, Show)

instance Eq Flag where
	(==) a b = text a == text b

-- defaults
newFlag text = Flag text Nothing Nothing "user"

setState  (Flag t st srv src) v = Flag t (Just v) srv src
setSource (Flag t st srv src) v = Flag t st srv   v
setService (Flag t st srv src) v = Flag t st (Just v) src

setStateS :: Flag -> String -> Flag
setStateS f = setState f . read

accept f = setState f Accepted
reject f = setState f Rejected

accepted  = maybe False stateAccepted  . state
rejected  = maybe False stateRejected  . state
processed = maybe False stateProcessed . state

--newFlag :: String -> Maybe Flag
--newFlag s = if isFlag s then Just $ Flag {text = s, state = Just New} else Nothing
