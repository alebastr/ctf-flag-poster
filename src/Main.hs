#!/usr/bin/runhaskell
module Main where
-- global imports
import System.IO
import Data.Maybe
import Control.Concurrent
import Control.Monad
import System.Log.Formatter
import System.Log.Logger
import System.Log.Handler (LogHandler, setFormatter)
import System.Log.Handler.Simple

-- local imports
import Flag
import Checker
import Checker.Connection.Socket
import Checker.Interface.Mock
import FlagReader
import Concurrency
import ServerSocket
import Storage
import Storage.DB
import Utils


checkerInterface = MockChecker

networkFlagReader :: FlagSubscriber -> NetworkHandler
networkFlagReader push addr msg = do 
	mapM_ push' $ extractFlags (flagFormat checkerInterface) msg 
	where 
		src = addr
		push' x = push $ flip setSource src $ newFlag x

mainConcurrent = do
	logHandlers <- sequence 
		[
		  verboseStreamHandler stderr DEBUG
		, fileHandler "poster.log" INFO
		]
		>>= mapM (return . (flip setFormatter $ simpleLogFormatter "[$time] $prio - $loggername: $msg"))
	updateGlobalLogger rootLoggerName (setLevel DEBUG . setHandlers logHandlers)
	updateGlobalLogger "flags" (setLevel DEBUG)
	logM "app" INFO "initializing"
	conn <- getCheckerConnectionSocket checkerInterface "127.0.0.1" 12345
	storage <- getStorageDB "db.sqlite"
	push <- startConcurrent
		[ -- preprocessing hooks
--		  update storage -- for 'pending' counter
		]
		[ -- chain of processors for flag
		  check storage
		, postFlag conn checkerInterface
		]
		[ -- subscribers - will receive message when processing succeeds
		--, log
		  update storage
		--, updateStat
		, (debugM "flags" . show)
		]
	forkIO $ networkListener 31337 $ networkFlagReader push
	-- just keep foreground process running
	interact id

main = mainConcurrent

