module Checker where
-- global imports
import Data.Maybe
import System.IO
import Text.Regex
-- local imports
import Flag

data Checker = Checker


class CheckerInterface a where
	flagFormat :: a -> Regex
	flagState  :: a -> String -> FlagState
	-- some behavior-controlling variables
	waitBeforeReconnect :: a -> IO ()
	waitBeforeSubmit 	:: a -> IO ()
	-- handshake with checker
	checkerConnectionInit :: a -> Handle -> IO ()

class CheckerConnection a where
	initConnection :: CheckerInterface b => a -> b -> IO a
	postFlag :: CheckerInterface b => a -> b -> FlagProcessor

-- default instance
instance CheckerConnection Checker where
	initConnection j i = return j
	postFlag _ _ = return . Just . accept
