module FlagReader where
-- global imports
import Data.Array;
import Text.Regex;
import Text.Regex.Posix;
-- local imports
import Flag;

extractFlags :: Regex -> String -> [String]
extractFlags pat s = map (fst . head . elems) $ matchAllText pat s
