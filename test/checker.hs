#!/usr/bin/runhaskell
-- quick and dirty checker
-- rejects all flags
import Control.Concurrent
import Control.Exception
import Network;
import System.IO
import Prelude hiding (catch)

networkListener :: Int -> IO ()
networkListener port = withSocketsDo $ do 
	sock <- listenOn $ PortNumber $ fromIntegral port
	listener sock
	where
		listener :: Socket -> IO ()
		listener mastersock = do
			(handle, host, port) <- accept mastersock
			forkIO $ connection handle host port 
			listener mastersock

		connection :: Handle -> HostName -> PortNumber -> IO ()
		connection h host port = do
			handle hErr $ do 
				hSetBuffering h LineBuffering
				processFlags h
			hClose h
			where
				hErr::SomeException -> IO ()
				hErr _ = return ()
		
		processFlags :: Handle -> IO ()
		processFlags h = do
			_ <- hGetLine h
			hPutStrLn h "Rejected"
			processFlags h

main = networkListener 12345
