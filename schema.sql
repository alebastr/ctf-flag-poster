CREATE TABLE IF NOT EXISTS flags (
		text TEXT PRIMARY KEY NOT NULL,
		state TEXT,
		source TEXT DEFAULT 'local',
		service TEXT DEFAULT '---',
		timestamp DEFAULT CURRENT_TIMESTAMP
	);
